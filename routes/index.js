var express = require('express');
var path = require('path');
var router = express.Router();
var app = express();     


/* GET home page. */
router.get('/', function(req, res) {
	if(typeof req.session !== 'undefined' && req.session.login){
		//si esta logeado
  		res.sendfile('messages.html', { root: path.join(__dirname, '../public') }); 
	}
	else{
		//no esta logeado
		res.redirect('/login'); 
	}
});

router.get('/logIn', function(req, res){ 
	if(typeof req.session !== 'undefined' && req.session.login){
		//si esta logeado
		res.redirect('/');   		
	}
	else{
		//no esta logeado
		res.sendfile('login.html', { root: path.join(__dirname, '../public') }); 
	}

});

router.post('/logIn', function(req, res){ 
	var user = require('./user'); 
	var name = req.body.usename; 
	var pass = req.body.password; 
	db.users.findOne({"firstName": name, "password":pass}, 
		function(err, doc){ 
			req.session.user = doc; 
			req.session.login = true; 
			res.redirect('/'); 
		}); 
}); 

module.exports = router;
